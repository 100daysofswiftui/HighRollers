//
//  HighRollersApp.swift
//  HighRollers
//
//  Created by Pascal Hintze on 21.03.2024.
//

import SwiftUI

@main
struct HighRollersApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
