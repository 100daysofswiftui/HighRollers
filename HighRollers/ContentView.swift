//
//  ContentView.swift
//  HighRollers
//
//  Created by Pascal Hintze on 21.03.2024.
//

import SwiftUI

struct RollResult: Codable, Identifiable, Hashable {
    var id = UUID()
    let score: Int
}

struct ContentView: View {
    @State private var numberOfDices = 4.0
    @State private var selectedDiceType = 6
    let possibleDices = [4, 6, 8, 10, 12, 20, 100]

    @State private var rolledTotal = 0
    @State private var rolledNumber = RollResult(score: 0)

    @State private var rolledNumbers = [RollResult]()

    let savePath = URL.documentsDirectory.appending(path: "SavedTotals.json")

    @State private var savedResults = [[RollResult]]()

    @State private var timeRemaining = 5
    let timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()

    let columns: [GridItem] = [.init(.adaptive(minimum: 60))]

    var body: some View {
        NavigationStack {
            Form {
                Section("Select number and type of dice:") {
                    VStack {
                        Text("Select number of dices:")

                        Slider(value: $numberOfDices, in: 1...12, step: 1)
                        Text("\(numberOfDices.formatted())")
                    }
                    .font(.title3)

                    HStack {
                        Picker("Select type:", selection: $selectedDiceType) {
                            ForEach(0..<possibleDices.count, id: \.self) { type in
                                Text("\(possibleDices[type])").tag(possibleDices[type])
                            }
                        }
                    }
                    .font(.title3)
                }

                Section("Lets roll:") {
                    Button {
                        timeRemaining = 5
                        rolledNumbers = []
                        rolledTotal = 0

                        for _ in 1...Int(numberOfDices) {
                            rolledNumber = RollResult(score: Int.random(in: 1...selectedDiceType))
                            rolledNumbers.append(rolledNumber)
                        }

                        for result in rolledNumbers {
                            rolledTotal += result.score
                        }
                        savedResults.insert(rolledNumbers, at: 0)
                        save()
                    } label: {
                        Label("Roll", systemImage: "dice")
                    }
                    .labelStyle(.iconOnly)
                    .background(.white)
                    .clipShape(.rect(cornerRadius: 10))
                    .font(.title)
                    .padding(5)
                    .frame(maxWidth: .infinity)
                }

                if rolledNumbers.isEmpty == false {
                    Section("You rolled:") {
                        HStack {
                            LazyVGrid(columns: columns) {
                                ForEach(rolledNumbers) { result in
                                    if timeRemaining > 0 {
                                        Text("\(Int.random(in: 1...selectedDiceType))")
                                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                                            .aspectRatio(1, contentMode: .fit)
                                            .foregroundStyle(.black)
                                            .background(.white)
                                            .clipShape(.rect(cornerRadius: 10))
                                            .shadow(radius: 3)
                                            .font(.title)
                                            .padding(5)


                                    } else {
                                        Text("\(result.score)")
                                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                                            .aspectRatio(1, contentMode: .fit)
                                            .foregroundStyle(.black)
                                            .background(.white)
                                            .clipShape(.rect(cornerRadius: 10))
                                            .shadow(radius: 3)
                                            .font(.title)
                                            .padding(5)

                                    }
                                }
                            }
                            .onReceive(timer) { _ in
                                timeRemaining -= 1
                            }
                        }

                        if rolledTotal != 0 {
                            Text("Total score: \(rolledTotal)")
                                .font(.title3)
                        }
                    }
                }

                if savedResults.isEmpty == false {
                    Section("Last results:") {
                        ForEach(savedResults, id: \.self) { result in
                            HStack {
                                LazyVGrid(columns: columns) {
                                    ForEach(result) { number in
                                        Text("\(number.score)")
                                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                                            .aspectRatio(1, contentMode: .fit)
                                            .foregroundStyle(.black)
                                            .background(.white)
                                            .clipShape(.rect(cornerRadius: 10))
                                            .shadow(radius: 3)
                                            .font(.title)
                                            .padding(5)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            .navigationTitle("High Rollers")
            .onAppear(perform: load)
            .sensoryFeedback(.impact, trigger: rolledNumber)
        }
    }

    func save() {
        if let data = try? JSONEncoder().encode(rolledTotal) {
            try? data.write(to: savePath, options: [.atomic, .completeFileProtection])
        }
    }

    func load() {
        if let data = try? Data(contentsOf: savePath) {
            if let results = try? JSONDecoder().decode([[RollResult]].self, from: data) {
                savedResults = results
            }
        }
    }
}

#Preview {
    ContentView()
}
