# HighRollers

HighRollers is an app that helps users roll dices and stores the results they had.

This app was developed as a challenge from the [100 Days of SwiftUI](https://www.hackingwithswift.com/guide/ios-swiftui/7/3/challenge) course from Hacking with Swift.